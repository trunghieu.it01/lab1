export const films = [
    {id: 1, image: 'images/ironman1.jpg', title: 'Iron Man', year: '2008', nation: 'USA'},
    {id: 2, image: 'images/hulk1.jpg', title: 'The Incredible Hulk', year: '2008', nation: 'USA'},
    {id: 3, image: 'images/ironman2.jpg', title: 'Iron Man 2', year: '2010', nation: 'USA'},
    {id: 4, image: 'images/thor1.jpg', title: 'Thor', year: '2011', nation: 'USA'},
    {id: 5, image: 'images/ca1.jpg', title: 'Captain America: The First Avenger', year: '2011', nation: 'USA'},
    {id: 6, image: 'images/avengers1.jpg', title: 'The Avengers', year: '2012', nation: 'USA'},
    {id: 7, image: 'images/ironman3.jpg', title: 'Iron Man 3', year: '2013', nation: 'USA'},
    {id: 8, image: 'images/thor2.jpg', title: 'Thor 2: The Dark World', year: '2013', nation: 'USA'},
    {id: 9, image: 'images/ca2.jpg', title: 'Captain America: The Winter Soldier', year: '2014', nation: 'USA'}
]